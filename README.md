Changelog :

    Added a ft_printbig function to support storage over int64_t using multiple int array.

Prototypes :

	ft_printbits(void *ptr, int size);
	ft_printbig(void *ptr, int size, int elem);

Parameters :

	ft_printbits takes two parameters :
		- a pointer to the address one desires to print bit by bit;
		- the size (in bytes) of the variable.
		
	ft_printbig takes three parameters :
	    - a pointer to the address one desires to print bit by bit;
	    - the size (in bytes) one desires to print;
	    - the size (in bytes) of array elements.

Return value :

	ft_printbits and ft_printbig are void functions and as such have no return values.

Use :

	Add ft_printbits.h to your .c file header.
	Make library ft_printbits.a with the make command.
	Compile main with library ft_printbits.a.
