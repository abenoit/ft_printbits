/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printbits.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/20 16:06:26 by abenoit           #+#    #+#             */
/*   Updated: 2019/12/29 16:13:32 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTBITS_H
# define FT_PRINTBITS_H

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>

void	ft_printbig(void *ptr, int size, int elem);
void	ft_printbits(void *ptr, int size);

#endif
