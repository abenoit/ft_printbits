/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printbits.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abenoit <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/20 16:08:10 by abenoit           #+#    #+#             */
/*   Updated: 2019/12/29 16:18:17 by abenoit          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printbits.h"

void	ft_printbits(void *ptr, int size)
{
	uint8_t		*data;
	size_t		count;
	uint8_t		i;
	uint8_t		lu;

	lu = 1;
	count = 0;
	data = (uint8_t*)(ptr) + (size - 1);
	while (count < size)
	{
		i = 0;
	while (i < 8)
	{
		if (*data & (lu << (7- i)))
			write(0, "1", 1);
		else
			write(0, "0", 1);
		i++;
	}
		data--;
		count++;
	}
}

void	ft_printbig(void *ptr, int size, int elem)
{
	uint8_t		*tmp;
	size_t		count;

	count = 0;
	tmp = ptr;
	while (count < size / elem)
	{
		ft_printbits(tmp, elem);
		tmp += elem;
		count++;
	}
}
